<%@ page language="java" contentType="text/html;charset=utf-8"  pageEncoding="utf-8" import="net.helloer.forum.cache.UserGroupCacheData"%>
<%
	int ugid = (Integer)session.getAttribute("uid");
	String context = request.getContextPath();
%>
<div id="menu">
	<ul>
		<%
			if( ugid == -1 ){   //表示游客
		%>
			<li>
				<a href="login.jsp">登录</a>
			</li>
			
			<li>
				<a href="register.jsp">注册</a>
			</li>
		<%
			} else {
				String uname = (String)session.getAttribute("uname");
		%>
				<li>
					<%
						if(ugid == 1){
					%>
							<a>管理员 : <b><%=uname %></b></a>
					<%
						} else {
					%>
						<a>会员 : <b><%=uname %></b></a>
					<% 		
						}
					%>
					
				</li>
				<li id="msg" class="dropmenu" onmouseover="showMenu(this.id)">
					<a href="usercenter.jsp?action=main">短信息</a>
				</li>
				<li id="control" class="dropmenu" onmouseover="showMenu(this.id)">
					<a href="usercenter.jsp?action=main">控制面版</a>
				</li>
				<% if( ugid == 8 ){ %>
				<li>
					<a href="systemlogin.jsp" target="_blank">系统设置</a>
				</li>
				<% } %>
				<li>
					<a href="logout.jsp">退出</a>
				</li>
		<%
			}
		%>
		<li>
			<a href="faq.jsp">帮助</a>
		</li>
	</ul>
</div>

<%
	if( ugid != -1 ){
%>
<div>
	<ul class="popupmenu_popup headermenu_popup" id="control_menu" style="display: none">
		<li>
			<a href="usercenter.jsp?action=control&type=userinfo">修改资料</a>
		</li>
		<li>
			<a href="usercenter.jsp?action=control&type=account">账户管理</a>
		</li>
		<li>
			<a href="usercenter.jsp?action=control&type=head">头像设置</a>
		</li>
		<li>
			<a href="usercenter.jsp?action=control&type=signature">签名设置</a>
		</li>
	</ul>
	<ul class="popupmenu_popup headermenu_popup" id="msg_menu" style="display: none">
		<li>
			<a href="msg.jsp">发短信</a>
		</li>
		<li>
			<a href="msg.jsp?action=rev">收件箱</a>
		</li>
		<li>
			<a href="msg.jsp?action=send">发件箱</a>
		</li>
	</ul>
</div>
<%
	}
%>

