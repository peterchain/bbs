<%@ page language="java" contentType="text/html;charset=utf-8"  pageEncoding="utf-8"%>
<%@page import="net.helloer.forum.cache.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>用户登录</title>
		<meta name="keywords" content="用户登录" />
		<meta name="descriptiobn" content="用户登录" />
		<meta name="generator" content="Helloer 2.0.0" />
		<meta name="author" content="Helloer Team" />
		<meta name="copyright" content="2008-2009 Helloer Inc." />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/style_common.css" />
		<script type="text/javascript">
		
			function loginCheck(){
				var username = document.login.username.value;
				var password = document.login.password.value;
				if(username==""){
					alert("请输入帐号!");
					document.login.username.focus();
					return false;
				}
				if(password==""){
					alert("请输密码!");
					document.login.password.focus();
					return false;
				}
				<%
					if(ConfigCacheData.getLogincode()==1){
				%>
				var code = document.login.code.value;
				if(code==""){
					alert("请输入验证码!");
					document.login.code.focus();
					return false;
				}
				<% 
					}
				%>
			}
			
		</script>
	</head>
	<body onload="document.login.username.focus();">
		<div class="wrap">
			<%@ include file="inc/head.jsp"%>
			<%@ include file="inc/usernav.jsp"%>
			<div id="foruminfo">
				<div id="userinfo">
					<div id="nav">
						<a href="<%=ctx%>/board/index.do">张桂芳个人论坛</a> &raquo;登录
					</div>
				</div>
			</div>
			<form method="post" name="login" action="<%=ctx%>/user/login.do" onsubmit="return loginCheck();">
				<input type="hidden" name="referer" value=""/>
				<div class="formbox" style="background: #FFF; border: 1px solid #CAD9EA; padding: 0px; margin-bottom: 10px;">
					<h1>
						登录
					</h1>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<th>
								<label>
									用户名
								</label>
							</th>
							<td>
								<input type="text" name="username" size="25" maxlength="40"/>
								<a href="register.jsp">立即注册</a>
							</td>
						</tr>
						<tr>
							<th>
								<label>
									密码
								</label>
							</th>
							<td>
								<input type="password" name="password" size="25" />
							</td>
						</tr>
						<%
							if(ConfigCacheData.getLogincode()==1){
						%>
						<tr>
							<th>
								<label>
									验证码
								</label>
							</th>
							<td>
								<script type="text/javascript">
									function changeCode(){
										var imga = document.getElementById("codeimage");
										imga.src = "code.jsp";
									}		
								</script>
								<input type="text" name="code" size="6"/> <a href="javascript:changeCode()"><img id="codeimage" src="code.jsp" align="middle"/></a>
							</td>
						</tr>
						<%
							}
						%>
						<%
							if(ConfigCacheData.getLoginquestion()==1){
						%>
						<tr>
							<th>
								<label>
									验证问题
								</label>
							</th>
							<td>
								<%=QuestionCacheData.getQuestion()%> <input type="text" name="answer" size="10"/>
							</td>
						</tr>
						<%
							}
						%>
						<tr>
							<th>
								登录有效期
							</th>
							<td>
								<label>
									<input class="radio" type="radio" name="cookietime" value="315360000" tabindex="8" />
									永久
								</label>
								<label>
									<input class="radio" type="radio" name="cookietime" value="2592000" tabindex="9" checked="checked" />
									一个月
								</label>
								<label>
									<input class="radio" type="radio" name="cookietime" value="86400" tabindex="10" />
									一天
								</label>
								<label>
									<input class="radio" type="radio" name="cookietime" value="3600" tabindex="11" />
									一小时
								</label>
								<label>
									<input class="radio" type="radio" name="cookietime" value="0" tabindex="12" />
									浏览器进程
								</label>
							</td>
						</tr>
						<tr>
							<th>
								&nbsp;
							</th>
							<td>
								<button class="submit" type="submit">
									提交
								</button>
							</td>
						</tr>
					</table>
				</div>
			</form>
			<%@ include file="inc/foot.jsp"%>
		</div>
	</body>
</html>