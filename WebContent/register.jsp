<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="net.helloer.forum.cache.ConfigCacheData"%>
<%
	String forumname = ConfigCacheData.getForumname();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>用户注册</title>
		<meta name="keywords" content="用户注册" />
		<meta name="descriptiobn" content="用户注册" />
		<meta name="generator" content="Common Team" />
		<meta name="author" content="Common Team" />
		<meta name="copyright" content="2008-2009 Helloer Inc." />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/style_common.css" />
		<script src="js/common.js" type="text/javascript"></script>
		<script src="js/jquery.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="wrap">
			<%@ include file="inc/head.jsp"%>
			<%@ include file="inc/usernav.jsp"%>
			<div id="foruminfo">
				<div id="userinfo">
					<div id="nav">
						<a href="<%=ctx%>/board/index.do">张桂芳个人论坛 </a>&raquo;注册
					</div>
				</div>
			</div>
			<% 
				if (ConfigCacheData.getOpenreg() == 0) { // 系统开启注册 
					if (ConfigCacheData.getShowdoc() == 0 || (request.getParameter("regdoc") != null && request.getParameter("regdoc").equals("true"))) { // 开启注册协议
			%>
			<form method="post" name="register" action="<%=ctx%>/user/regist.do"  onsubmit="return registerCheck();">
				<div class="formbox" style="background: #FFF; border: 1px solid #CAD9EA; padding: 0px; margin-bottom: 10px;">
					<h1>
						注册
					</h1>
					<table cellspacing="0" cellpadding="0" width="100%" >
						<tr>
							<th>
								<label>
									用户名
								</label>
							</th>
							<td>
								<input type="text" id="username" name="username" size="25" maxlength="20" value=""/> 用户名长度在2-20之间  	
							</td>
						</tr>
						<tr>
							<th>
								<label>
									密码
								</label>
							</th>
							<td>
								<input type="password" name="password" size="25"/>  两次密码必须一致
							</td>
						</tr>

						<tr>
							<th>
								<label>
									确认密码
								</label>
							</th>
							<td>
								<input type="password" name="repassword" size="25"/>
							</td>
						</tr>
						<tr>
							<th>
								<label>
									Email
								</label>
							</th>
							<td>
								<input type="text" name="email" size="25" />  您的Email地址
							</td>
						</tr>
						<%
							if(ConfigCacheData.getRegcode()==1){
						%>
						<tr>
							<th>
								<label>
									验证码
								</label>
							</th>
							<td>
								<script type="text/javascript">
									function changeCode(){
										var imga = document.getElementById("codeimage");
										imga.src = "code.jsp";
									}		
								</script>
								<input type="text" name="code" size="6"/> <a href="javascript:changeCode()"><img id="codeimage" src="code.jsp" align="middle"/></a>
							</td>
						</tr>
						<%
							}
						%>
						<%
							if(ConfigCacheData.getRegquestion()==1){
						%>
						<tr>
							<th>
								<label>
									验证问题
								</label>
							</th>
							<td>
								<%=QuestionCacheData.getQuestion()%> <input type="text" name="answer" size="10"/>
							</td>
						</tr>
						<%
							}
						%>
						<tr>
							<th>
								&nbsp;
							</th>
							<td>
								<button class="submit" type="button" id="reg-btn">
									提交
								</button>
							</td>
						</tr>
					</table>
				</div>
				<input name="action" type="hidden" value="reg"/>
			</form>
			<%
				} else {
			%>
			<form method="post" action="register.jsp">
				<div class="mainbox formbox">
					<h1>
						注册协议
					</h1>
					<table cellspacing="0" cellpadding="0" width="100%" align="center" class="register">
						<tbody>
							<tr>
								<td>
									<br />
										<%=ConfigCacheData.getDoccontext()%>
									<br />
									<br />
								</td>
							</tr>
						</tbody>
						<tr class="btns" style="height: 40px">
							<td align="center" id="rulebutton">
								<button type="submit">同 意</button> &nbsp; <button type="button" onclick="location.href='index.jsp'">不同意</button>
							</td>
						</tr>
<!--						<input type="hidden" name="regdoc" value="true"/>-->
					</table>
				</div>
			</form>
			<%
					}
				} else {
			%>
			<div class="box message">
				<h1>
					Helloer 提示信息
				</h1>
				<p>
					<b>对不起! 论坛暂时关闭注册。</b>
				</p>
			</div>
			<%
				}
			%>
			<%@ include file="inc/foot.jsp"%>
		</div>
		
		<script type="text/javascript">
		
			function registerCheck(){
				var username = document.register.username.value;
				var password = document.register.password.value;
				var repassword = document.register.repassword.value;
				var email = document.register.email.value;
				if(username==""){
					alert("请输入帐号!");
					document.register.username.focus();
					return false;
				}
				if(username.length<2||username.length>20){  
					alert("用户名长度在2-20字符之间!");
					return false;
				}
				if(password==""){
					alert("请输密码!");
					document.register.password.focus();
					return false;
				}
				if(repassword==""){
					alert("请输确认密码!");
					document.register.repassword.focus();
					return false;
				}
				if(password!=repassword){
					alert("密码和确认密码不一致!");
					return false;
				}
				if(!isEmail(email)){
					alert("Email格式不正确!");
					document.register.email.focus();
					return false;
				}
				return true;
			}
			
			function isEmail(string){
   				var partn=/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/;
    			if (!partn.exec(string))
       				 return false;
    			return true;
			}
			
			 $(document).ready(function (e) {
				 
				 $("#reg-btn").click(function(){
					 var flag = registerCheck();
					 if(flag){
							var username = document.register.username.value;
							var password = document.register.password.value;
							var repassword = document.register.repassword.value;
							var email = document.register.email.value;
						 $.ajax({
					            type: "post",
					            url: "<%=ctx%>/user/regist.do?username="+username+"&password="+password+"&repassword="+repassword+"&email="+email,
					            //data:JSON.stringify(param),
					            contentType: "application/json; charset=utf-8",
					            dataType: "json",
					            error:function(XMLHttpRequest, textStatus, errorThrown){			//网络异常
					                console.log("contact.js|loadContact error|"+textStatus);
					            },
					            beforeSend: function(request) {
					                request.setRequestHeader("auth", "Chenxizhang");
					            },
					            success: function(res){
					                //把联系人信息保存在本地
					                if(res != undefined){
					                	var code = res.result.retCode;
					                	if(code != "0"){
					                		alert(res.result.retDesc);
					                	} else {
					                		window.location.href = "login.jsp";
					                	}
					                }
					            }
					        }) ;
					 } else {
						 return;
					 }
				 });
			 });
			
		</script>
	</body>
</html>