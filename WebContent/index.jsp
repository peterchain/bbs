<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="net.helloer.forum.pojo.*,net.helloer.forum.cache.*,net.helloer.forum.online.OnlineCount,java.util.*,net.helloer.common.util.*"%>
<%
	String forumname = ConfigCacheData.getForumname();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>论坛首页</title>
		<meta name="keywords" content="Helloer" />
		<meta name="descriptiobn" content="Helloer" />
		<meta name="generator" content="Helloer 2.0.0" />
		<meta name="author" content="Helloer Team" />
		<meta name="copyright" content="2008-2009 Helloer Inc." />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/style_common.css" />
		<script src="js/common.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="wrap">
			<%@ include file="inc/head.jsp"%>
			<%@ include file="inc/usernav.jsp"%>
			<div id="foruminfo">
				<div id="userinfo">
					<div id="nav">
						<a href="index.jsp">张桂芳个人论坛</a> &raquo; 欢迎光临，现在是 <script type="text/javascript">var date= new Date(); document.write(date.toLocaleString());	</script>
					</div>
				</div>
				<div id="forumstats">
					今日:<em>4</em>, 昨日:<em>5</em>, 主题:<em>2</em>, 帖子:<em>100</em>,	会员:<em>10</em>,	新会员:<a href="" target="_blank">王宁波</a>&nbsp;<a href="rss.jsp"><img src="images/icon/rss.gif" align="middle"/></a>
				</div>
			</div>
			
			<!--  板块列表开始 -->
			<div id="forumlist">
				<div class="mainbox forumlist">
					<span class="headactions"> 
	 				分区版主:
	 				<a href="">张桂芳</a>
	 				</span>
					<h3 style="letter-spacing: 2px">
						我的心情日记
					</h3>
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>
									论坛
								</th>
								<td class="num">
									主题/帖子
								</td>
								<td class="last">
									最后发表
								</td>
								<td class="master">
									版主
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th class="new">
									<a href="forum-1-1.html" class="name" style="color:black">私人笔记</a> <span class="todaynum">(今日:3)</span>
									<p>我的私人笔记</p>
								</th>
								<td class="num"> 
									1 /	3
								</td>
								<td class="last">
									<p>
										<a href="thread-1-1.html">ssh</a>
									</p>
									<p class="moderators">
										By: <a href="user-2.html" target="_blank">peter</a> - <span class="time">2015-9-22 14:35:20</span>
									</p>
								</td>
								<td class="master">
									<p>
										<a href="user-1.html">张桂芳</a>
									</p>
								</td>
							</tr>
							
							<tr>
								<th class="new">
									<a href="forum-1-1.html" class="name" style="color:black">心情日记</a> <span class="todaynum">(今日:3)</span>
									<p>我的心路历程</p>
								</th>
								<td class="num"> 
									1 /	3
								</td>
								<td class="last">
									<p>
										<a href="thread-1-1.html">ssh</a>
									</p>
									<p class="moderators">
										By: <a href="user-2.html" target="_blank">peter</a> - <span class="time">2015-9-22 14:35:20</span>
									</p>
								</td>
								<td class="master">
									<p>
										<a href="user-1.html">张桂芳</a>
									</p>
								</td>
							</tr>
							
							<tr>
								<th class="new">
									<a href="forum-1-1.html" class="name" style="color:black">我的故事</a> <span class="todaynum">(今日:3)</span>
									<p>我的故事</p>
								</th>
								<td class="num"> 
									1 /	3
								</td>
								<td class="last">
									<p>
										<a href="thread-1-1.html">ssh</a>
									</p>
									<p class="moderators">
										By: <a href="user-2.html" target="_blank">peter</a> - <span class="time">2015-9-22 14:35:20</span>
									</p>
								</td>
								<td class="master">
									<p>
										<a href="user-1.html">张桂芳</a>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- 模板列表结束 -->
			
			
<!--			<div class="box">-->
<!--				<h4>-->
<!--					友情链接-->
<!--				</h4>-->
<!--				<table id="forumlinks" cellpadding="0" cellspacing="0" style="table-layout: fixed;">-->
<!--					<tr>-->
<!--						<td>-->
<!--							<a href="http://www.helloer.net" title="Helloer官方网站" target="_blank">Helloer官方网站</a>-->
<!--						</td>-->
<!--					</tr>-->
<!--				</table>-->
<!--			</div>-->

			<div class="box" id="online">
				<h4>
					<strong><a>在线会员</a> </strong> - <em>10</em> 人在线 - <em>3</em> 会员,<em>1</em> 位游客 - 最高记录是 <em>1</em> 于 <em> 2008-3-15 0:00:00</em>.
				</h4>
				<dl id="onlinelist">
					<dt>
						<img src="images/cutline/admin.gif"/> 管理员&nbsp; &nbsp; &nbsp; 
						<img src="images/cutline/super.gif"/> 超级版主&nbsp; &nbsp; &nbsp; 
						<img src="images/cutline/master.gif"/> 版主&nbsp; &nbsp; &nbsp; 
						<img src="images/cutline/member.gif"/> 会员&nbsp; &nbsp; &nbsp; 
					</dt>
					<dd>
						<ul class="userlist">
						
						</ul>
					</dd>
				</dl>
			</div>
<!--			<div class="legend">-->
<!--				<label>-->
<!--					<img src="images/icon/topic_permit.gif" alt="有新帖的版块" />-->
<!--					有新帖-->
<!--				</label>-->
<!--				<label>-->
<!--					<img src="images/icon/topic_unpermit.gif" alt="无新帖的版块" />-->
<!--					无新帖-->
<!--				</label>-->
<!--			</div>-->
			<%@ include file="inc/foot.jsp"%>
		</div>
	</body>
</html>