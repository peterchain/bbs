package com.zgf.bbs.service;

import com.zgf.bbs.beans.UserInfo;
import com.zgf.bbs.beans.req.Result;
import com.zgf.bbs.beans.rsp.LoginRsp;

public interface IUserService {
	/**
	 * 用户登录   
	 * @author Administrator
	 * @param userName 用户名
	 * @param passwd   密码
	 * */
	LoginRsp doLogin(String userName,String passwd);
	
	/**
	 * 用户注册
	 * @author Administrator
	 * @param userName 用户名
	 * @param passwd 密码
	 * @param ensurePasswd 确认密码
	 * @return
	 */
	Result doRegist(String userName,String password,String ensurePasswd,String email,String regIp);
	
	/**
	 * 更新用户详细信息
	 * @author Administrator
	 * @param userInfo  用户信息
	 * @return
	 */
	public boolean updateProfile(UserInfo userInfo);
}
