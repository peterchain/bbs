package com.zgf.bbs.service.impl;

import com.zgf.bbs.beans.User;
import com.zgf.bbs.beans.UserInfo;
import com.zgf.bbs.beans.req.Result;
import com.zgf.bbs.beans.rsp.LoginRsp;
import com.zgf.bbs.cache.BbsConfigCacheData;
import com.zgf.bbs.cache.NewlyUserCache;
import com.zgf.bbs.constant.ErrorCode;
import com.zgf.bbs.dao.UserDao;
import com.zgf.bbs.dao.impl.UserDaoImpl;
import com.zgf.bbs.service.IUserService;

public class UserServiceImpl implements IUserService {

	@Override
	public LoginRsp doLogin(String userName, String passwd) {
		LoginRsp rsp = new LoginRsp();
		Result result = null;
		UserDao dao = new UserDaoImpl();
		User user = dao.queryByUser(userName);
		if(null == user){
			result = new Result(ErrorCode.USERNAME_INVALID);
			rsp.setResult(result);
			return rsp;
		}
		
		if(!user.getPassword().equals(passwd)){
			result = new Result(ErrorCode.PASSWORD_INCORECT);
			rsp.setResult(result);
			return rsp;
		}
		
		result = new Result();
		rsp.setResult(result);
		rsp.setUser(user);
		return rsp;
	}

	@Override
	public boolean updateProfile(UserInfo userInfo) {
		return false;
	}

	@Override
	public Result doRegist(String userName, String password, String ensurePasswd,String email,String regIp) {
		
		Result result = null;
		
		int i = -1;
		UserDao dao = new UserDaoImpl();
		
		if(null !=dao.queryByUser(userName)){
			result = new Result(ErrorCode.USERNAME_REPEATED);
			return result;
		}
		
		if(!password.equals(ensurePasswd)){
			result = new Result(ErrorCode.PASSWORD_INCORECT);
			return result;
		}
		int uid = BbsConfigCacheData.getNextUid();
		i = dao.createUser(uid,userName, password, email,regIp);
		if(i != -1){
			dao.createUserProfile(uid,email);
		}
		result = new Result();
		
		User u = new User();
		u.setPassword(password);
		u.setRegDate(System.currentTimeMillis());
		u.setUserId(uid);
		u.setUserName(userName);
		NewlyUserCache.setUser(u);//设置最新会员信息
		return result;
	}

}
