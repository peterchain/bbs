package com.zgf.bbs.dao;

import com.zgf.bbs.beans.User;
import com.zgf.bbs.beans.UserInfo;

public interface UserDao {
	
	
	/**
	 * 通过密码查询用户信息
	 * @param passwd
	 */
	public User queryByUser(String username);
	
	/**
	 * 创建账户
	 * @param userName
	 * @param passwd
	 * @param email
	 * @param regIp
	 * @return
	 */
	public int createUser(Integer uid,String userName,String passwd,String email,String regIp);
	
	/**
	 * 创建用户详情
	 * @param userId
	 * @param email
	 * @return
	 */
	public int createUserProfile(Integer uid,String email);
	
	/**
	 * 更新用户详细信息
	 * @param userInfo
	 * @return
	 */
	public int updateUserPrtofile(UserInfo userInfo);
	
	public int getMaxUid();
}
