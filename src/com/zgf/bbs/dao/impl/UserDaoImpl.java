package com.zgf.bbs.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.zgf.bbs.beans.User;
import com.zgf.bbs.beans.UserInfo;
import com.zgf.bbs.dao.UserDao;
import com.zgf.bbs.manager.DbConnectionManager;

public class UserDaoImpl implements UserDao{

	@Override
	public User queryByUser(String username){
		User user = null;
		Connection dbConn = DbConnectionManager.getConnection();
		ResultSet rs = null;
		String sql = "select * from t_user where user_name='"+username+"'";
		Statement pstmt = null;
		try {
			pstmt = dbConn.createStatement();
			rs = pstmt.executeQuery(sql);
			while(rs.next()){
				user = new User();
				user.setUserId(rs.getInt("user_id"));
				user.setUserName(rs.getString("user_name"));
				user.setPassword(rs.getString("password"));
				user.setRegDate(rs.getLong("reg_date"));
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DbConnectionManager.closeAll(dbConn, rs, pstmt, null);
		}
		return user;
	}

	public int createUser(Integer uid,String userName,String passwd,String email,String regIp) {
		int i = -1;
		Connection dbConn = DbConnectionManager.getConnection();
		String sql = "insert into t_user (user_id,user_name,password,email,reg_ip,reg_date) values (?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		try {
			pstmt = dbConn.prepareStatement(sql);
			
			pstmt.setInt(1, uid);
			pstmt.setString(2, userName);
			pstmt.setString(3, passwd);
			pstmt.setString(4, email);
			pstmt.setString(5, regIp);
			pstmt.setLong(6, System.currentTimeMillis());
			pstmt.execute();
			i = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DbConnectionManager.closeAll(dbConn, null, pstmt, null);
		}
		return i;
	}

	@Override
	public int createUserProfile(Integer uid,String email) {
		int i = -1;
		Connection dbConn = DbConnectionManager.getConnection();
		String sql = "insert into t_user_info (user_id,email,headpic) values (?,?,?)";
		PreparedStatement pstmt = null;
		try {
			pstmt = dbConn.prepareStatement(sql);
			pstmt.setInt(1, uid);
			pstmt.setString(2, email);
			pstmt.setString(3, "man01.gif");//设置默认头像
			pstmt.execute();
			i = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DbConnectionManager.closeAll(dbConn, null, pstmt, null);
		}
		return i;
	}

	@Override
	public int updateUserPrtofile(UserInfo userInfo) {
		return 0;
	}

	@Override
	public int getMaxUid() {
		int msxUid = 0;
		Connection dbConn = DbConnectionManager.getConnection();
		ResultSet rs = null;
		String sql = "select MAX(user_id) from t_user";
		Statement pstmt = null;
		try {
			pstmt = dbConn.createStatement();
			rs = pstmt.executeQuery(sql);
			while(rs.next()){
				msxUid = rs.getInt(1);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DbConnectionManager.closeAll(dbConn, rs, pstmt, null);
		}
		return msxUid;
	}
}
