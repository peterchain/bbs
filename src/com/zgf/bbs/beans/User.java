package com.zgf.bbs.beans;

public class User{
	private Integer userId;
	private String userName;
	private String password;
	private String regIp; //注册机子IP
	private Long regDate;//注册时间
	private String loginIp; //登录所在机子IP
	private Long loginDate;//登录时间
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRegIp() {
		return regIp;
	}
	public void setRegIp(String regIp) {
		this.regIp = regIp;
	}
	public Long getRegDate() {
		return regDate;
	}
	public void setRegDate(Long regDate) {
		this.regDate = regDate;
	}
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	public Long getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(Long loginDate) {
		this.loginDate = loginDate;
	}
	
}
