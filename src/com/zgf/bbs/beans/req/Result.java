package com.zgf.bbs.beans.req;

import com.zgf.bbs.constant.ErrorCode;

public class Result {
	private String retCode;
	private String retDesc;
	
	public Result(){
		this.retCode = ErrorCode.SUCCESS.getErrorcode();
		this.retDesc = ErrorCode.SUCCESS.getErrormsg();
	}
	
	public Result(String code, String message){
		this.retCode = code;
		this.retDesc = message;
	}
	
	public Result(ErrorCode errorCode){
		this.retCode = errorCode.getErrorcode();
		this.retDesc = errorCode.getErrormsg();
	}
	
	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetDesc() {
		return retDesc;
	}
	public void setRetDesc(String retDesc) {
		this.retDesc = retDesc;
	}
}
