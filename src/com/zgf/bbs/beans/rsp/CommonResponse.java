package com.zgf.bbs.beans.rsp;

import com.zgf.bbs.beans.req.Result;

public class CommonResponse {
	private Result result;
	private Object data;
	public Result getResult() {
		return result;
	}
	public void setResult(Result result) {
		this.result = result;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
