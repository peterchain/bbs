package com.zgf.bbs.beans.rsp;

import com.zgf.bbs.beans.User;
import com.zgf.bbs.beans.req.Result;

public class LoginRsp {
	private User user;
	private Result result;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Result getResult() {
		return result;
	}
	public void setResult(Result result) {
		this.result = result;
	}
}
