package com.zgf.bbs.cache;

import com.zgf.bbs.beans.User;

public class NewlyUserCache {
	private static  User user;
	
	public static void setUser(User u){
		user = u;
	}
	
	public static User getUser(){
		return user;
	}
	
}
