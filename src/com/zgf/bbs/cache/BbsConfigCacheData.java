package com.zgf.bbs.cache;

public class BbsConfigCacheData {
	private static int currentMaxUid;
	private static String loginUser;
    private static String forumname;
    private static boolean isAdmin;
    
    public static synchronized void setCurrentMaxUid(int uid){
    	currentMaxUid = uid;
    }
    
    public static synchronized int getNextUid(){
    	currentMaxUid ++ ;//自增
    	return currentMaxUid;
    }
    
    public static synchronized int getCurrentMaxUid(){
    	return currentMaxUid;
    }
    
    public static void setForumname(String forumName){
    	forumname = forumName;
    }
    
    public static String getForumname(){
    	return forumname;
    }
    
    public static void setIsAdmin(boolean flag){
    	isAdmin = flag;
    }
    
    public static boolean getIsAdmin(){
    	return isAdmin;
    }
    
    public static void setLoginUser(String user){
    	loginUser = user;
    }
    
    public static String getLoginUser(){
    	return loginUser;
    }

	public BbsConfigCacheData() {
	}
}
