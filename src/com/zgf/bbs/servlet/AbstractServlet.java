package com.zgf.bbs.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AbstractServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AbstractServlet() {
        super();
    }
	/**
	 * 
	 */
//	private static final Logger log = LoggerFactory
//			.getLogger(AbstractServlet.class);

	protected Gson gson;

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		gson = new GsonBuilder()
				.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT) // 不导出实体中没有用@Expose注解的属性
				.enableComplexMapKeySerialization() // 支持Map的key为复杂对象的形式
				.setPrettyPrinting() // 对json结果格式化.
				.create();
	}

	public Gson getGson() {
		if (null == gson) {
			gson = new GsonBuilder()
					.excludeFieldsWithModifiers(Modifier.FINAL,
							Modifier.TRANSIENT) // 不导出实体中没有用@Expose注解的属性
					.enableComplexMapKeySerialization() // 支持Map的key为复杂对象的形式
					.setPrettyPrinting() // 对json结果格式化.
					.create();
		}
		return gson;
	}

	public String readJSONString(HttpServletRequest request) {
		StringBuffer json = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				json.append(line);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return json.toString();
	}

	public void flush(HttpServletResponse resp) throws IOException {
		resp.getWriter().flush();
		resp.getWriter().close();
	}

	public void writeRspJson(HttpServletResponse resp, String jsonRsp)
			throws IOException {
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		resp.getWriter().write(jsonRsp);
	}

	/**
	 * 向响应中输出内容
	 * 
	 * @param rspStr
	 * @param resp
	 * @throws IOException
	 */
	protected void writeRspJson(HttpServletResponse resp, String rspStr,
			long start) throws IOException {
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		PrintWriter pw = null;
//		if (log.isDebugEnabled()) {
//			log.debug("Http Rsp:" + rspStr);
//		}
		try {
			long end = System.currentTimeMillis();
			long x = end - start;
			if (x > 1000) {
//				if (log.isInfoEnabled()) {
//					log.info("exit servlet " + this.getClass().getName()
//							+ ".time " + x + " ms");
//				}
			}
			pw = resp.getWriter();
			pw.println(rspStr);
		} catch (IOException e) {
			throw e;
		} finally {
			if (pw != null) {
				pw.flush();
				pw.close();
			}
		}
	}

	protected boolean authHeader(HttpServletRequest request) {
		return true;
	}

	/**
	 * 判断请求来源是否安全。
	 * 
	 * @param request
	 * @return
	 */
	protected boolean isRequestSafe(HttpServletRequest request) {
		String remoteAttr = request.getRemoteAddr();
		System.out.println("remoteAttr = " + remoteAttr);
		
//		if (log.isDebugEnabled()) {
//			log.debug("alipay remoteAttr:" + remoteAttr);
//		}

		String remoteHost = request.getRemoteHost();
		System.out.println("remoteIP = " + remoteHost);
		
//		if (log.isDebugEnabled()) {
//			log.debug("alipay remoteIP:" + remoteHost);
//		}
		boolean flag = false;

		// 对请求头部的Host进行判断加固安全问题DNS pinning 2012-10-16
		String hostIP = request.getHeader("Host");
		System.out.println("host = " + hostIP);
		
//		if (log.isDebugEnabled()) {
//			log.debug("alipay hostIP:" + hostIP);
//		}
		
		if (StringUtils.isNotEmpty(hostIP)) {
			if (hostIP.indexOf(":") > 0) {
				hostIP = hostIP.substring(0, hostIP.indexOf(":"));
			}
			if (hostIP.equals("ali.pay.com")) {
				flag = true;
			}
		}
		return flag;
	}
	
	public String getActionName(HttpServletRequest request){
		String uri = request.getRequestURI();
		String action = uri.substring(uri.lastIndexOf("/")+1).split("\\.")[0];
		System.out.println(action);
		return action;
	}

}
