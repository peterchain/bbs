package com.zgf.bbs.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zgf.bbs.beans.req.Result;
import com.zgf.bbs.beans.rsp.CommonResponse;
import com.zgf.bbs.beans.rsp.LoginRsp;
import com.zgf.bbs.constant.ActionType;
import com.zgf.bbs.constant.ErrorCode;
import com.zgf.bbs.service.IUserService;
import com.zgf.bbs.service.impl.UserServiceImpl;

public class UserControllerServlet extends AbstractServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6375277597914807049L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = getActionName(req);
		if(ActionType.ACTION_REGIST.equals(action)){
			//用户注册
			userRegist(req, resp);
		} else if(ActionType.ASCTION_LOGIN.equals(action)){
			//用户登录
			userLogin(req, resp);
		}
	}
	
	
	private void userRegist(HttpServletRequest req, HttpServletResponse resp){
		String userName = req.getParameter("username");
		String password = req.getParameter("password");
		String ensurePassword = req.getParameter("repassword");
		String email = req.getParameter("email");
		IUserService userService = new UserServiceImpl();
		String ip = req.getRemoteHost();
		try {
			Result result = userService.doRegist(userName, password,ensurePassword,email,ip);
			CommonResponse response = new CommonResponse();
			response.setResult(result);
			if(result.getRetCode().equals(ErrorCode.SUCCESS.getErrorcode())){
				response.setData(userName);
			}
			writeRspJson(resp,gson.toJson(response));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void userLogin(HttpServletRequest req, HttpServletResponse resp){
		String userName = req.getParameter("username");
		String passwd = req.getParameter("password");
		try {
			IUserService userService = new UserServiceImpl();
			LoginRsp result = userService.doLogin(userName, passwd);
			if(result.getResult().getRetCode().equals(ErrorCode.SUCCESS.getErrorcode())){
				req.getSession().setAttribute("uid", result.getUser().getUserId());
				req.getSession().setAttribute("uname", result.getUser().getUserName());
			}
			resp.sendRedirect("../index.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
