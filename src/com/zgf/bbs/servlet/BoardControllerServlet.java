package com.zgf.bbs.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zgf.bbs.constant.ActionType;

public class BoardControllerServlet extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3713034199850633428L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = getActionName(req);
		if(ActionType.ACTION_BOARD_INDEX.equals(action)){
			//用户注册
			index(req, resp);
		} else if(ActionType.ACTION_BOARD_VISITOR.equals(action)){
			visitor(req, resp);
		}
	}
	
	private void visitor(HttpServletRequest req, HttpServletResponse resp){
		//获取数据，返回到index界面
		try {
			req.getSession().setAttribute("uid",-1);
			resp.sendRedirect("../index.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void index(HttpServletRequest req, HttpServletResponse resp){
		//获取数据，返回到index界面
		try {
			resp.sendRedirect("../index.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
