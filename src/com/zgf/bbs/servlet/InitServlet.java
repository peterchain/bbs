package com.zgf.bbs.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.zgf.bbs.cache.BbsConfigCacheData;
import com.zgf.bbs.dao.impl.UserDaoImpl;

public class InitServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4647001406270671067L;

	public static final String FILE_SEPARATOR = System.getProperties()
			.getProperty("file.separator");

	private static String contextPath;

	private static String classPath;

	@Override
	public void init(ServletConfig config) throws ServletException {

		super.init(config);

		String prefix = config.getServletContext().getRealPath("/");
		InitServlet.contextPath = prefix;

		classPath = Thread.currentThread().getContextClassLoader()
				.getResource("").getPath();
		
		int maxUid = new UserDaoImpl().getMaxUid();
		BbsConfigCacheData.setCurrentMaxUid(maxUid);
	}

	@Override
	public void destroy() {
	}

	public static final String getContextPath() {
		return InitServlet.contextPath;
	}

	public static final String getClassPath() {
		return classPath;
	}
}
