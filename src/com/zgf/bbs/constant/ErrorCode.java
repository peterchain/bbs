package com.zgf.bbs.constant;

public enum ErrorCode {
	SUCCESS("0","success"),
	USERNAME_REPEATED("400001","用户名重复,请重新输入!"),
	PASSWORD_INCORECT("400002","密码不一致,请重新输入!"),
	
	VERIFY_CODE_INVALID("400003","验证码错误"),
	
	USERNAME_INVALID("400004","用户名错误"),
	
	PASSWD_INVALID("400005","密码错误");
	
	private String errorcode;
	private String errormsg;
	
	ErrorCode(String errorcode, String errormsg){
		this.errorcode = errorcode;
		this.errormsg = errormsg;
	}
	
	public String getErrorcode(){
		return errorcode;
	}
	
	public String getErrormsg(){
		return errormsg;
	}
}